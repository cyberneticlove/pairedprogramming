#Paired Programming Sessions
The goal of these paired programming sessions is hands-on experience with Jasmine Unit Testing and the 2014 MCOM UI Architecture.

Feedback is always welcome to improve these sessions.

Every other week will target a specific skill or aspect of Jasmine Testing. In between, there will be office hours to cover questions that come up, or to work on whatever interests participants. For example, we might work on updating utilities and components in macysJS, or set up environments.

##January 23, 2015
###Attendees
James, Thaddeus, feliciA

###Summary
* James continued with workflow on coverage and testing, sent Brenda a patch, because coverage is not increasing despite additional tests
* feliciA and Thad are almost done setting up MacysUI. It seems that there are different permissions settings for their machines that are preventing require configs from working. 
* Need to go through files in MacysUI and change owner to self (account name/RACFID) whenever it is different

##January 16, 2015
###Attendees
Matthew, James, Thaddeus, Hans, Kristine

###Summary
* Stepped through workflow of building tests and coverage reports
* Thaddeus documented grunt commands and when to use them
* Paired up and successfully added passing tests following surrounding patterns of Require modules
* Found an error in coverage reports not updating – Brenda will follow up with Joe

##January 9, 2015
###Attendees
Alex, Matt, Matthew, James, Thaddeus

###Summary
* Started setting up MacysUI in order to prepare for pairing up to increase test coverage of the repository
* Used brew and npm to get all required packages
* Installed Compass and grunt-cli globally
* Updated our .m2/ directories per the README in the root of MacysUI
* Ran mvn clean install from root of directories
* Matthew made it to the last step and was able to run grunt connect to look at coverage metrics

##December 12, 2014
###Attendees
Divya

###Summary
* Intro to Node.js

##December 4, 2014
###Attendees
Hans

###Summary
* reviewed Objects, their properties, and methods, along with `this`

###Attendees
Hans, James, Mike, Matthew, Divya, Alex, Matt

###Instructions
√  Make sure you have jQuery, Backbone, and Underscore loaded in SpecRunner.html

Write tests in spec/ and define your Backbone Model in src/
√  A Backbone Model in the global scope with default properties of 

√  originalPrice
√  salePrice

then create a method calculatePercent that calculates the percentage of the sale price, returns the percentage, and sets a property called salePercent

make sure that method is called during initialization  

##November 21, 2014
###Attendees
Divya, Matt, Hans

###Summary
* Hans worked on Objects
* Divya reviewed a design for using jQuery carousel
* Matt and Divya wrote tests for a zip code function

##November 14, 2014
###Attendees
Divya, Alex, Matt, Matthew

###Summary
* Continued working on Matthew's expandable banner. We discussed scaffolding, updated tests, ran into DOM issues, discussed, ended on one blocker where height was being set inline dynamically

##November 7, 2014
###Starting from the beginning
###Attendees
Divya, Alex, Matt, Hans, feliciA

###Summary
* Reviewed matchers and paired up to write tests and solidify understanding of directory structures
* Questions came up about strict equality, memory usage, and the difference between `toBeTruthy`, `toBe`, and `toEqual`

###Attendees
James, Divya, Alex, Matt, feliciA, Hans

###Summary
* Reviewed how to define and instantiate a View
* Reviewed how to instantiate a view with a model
	1. Define a Model
	2. instantiate a model
	3. Define a View
	4. instantiate a view and associate it with a model

###Links
[Divya and James](http://jsbin.com/suxunotapo/1/), [editable version](http://jsbin.com/suxunotapo/1/edit?html,js,console,output)

##October 24, 2014
###Starting from the beginning
###Attendees
Divya, Matthew, feliciA

###Summary
* reviewed basic matchers `toEqual` `toBeGreaterThan` and `toBeLessThan`, `toMatch`, `toBeDefined`, as well as how to use `.not`
* discussed the merits of testing on data vs presentation
* talked about application of testing in Coremetrics

###Attendees
Mike, Alex, feliciA, Matthew, Hans, James, Divya, Matt

###Summary
* learned the basics of Backbone Views - defining and instantiating, `render` method, returning `this` in the render method, and associating a Backbone View with a Backbone Model
* continued to look at Matthew's code for expandable banners
* downloaded Jasmine 2.0.3
* set up testing directory structure in Matthew's files

##October 17, 2014
###Attendees
feliciA, Mike, Divya, Matthew, James

###Summary 
* looked at Matthew's refactor of expandable banner
* converted it to working Backbone View
* discussed testing steps and approach
* testing approach for expand:
	1) set height and check for correct height
	2) make sure newheight is greater than old height
	3) set spy and make sure final function is called

##October 3, 2014
###Starting from the beginning
###Attendees
Hans, feliciA, Alex, Matt, Divya

####Summary
* read Chapter 1 from Testable JavaScript
* discussed the following:
	* What is TDD
	* What is BDD
	* What do TDD/BDD have to do with Agile?
	* What is the goal?
	* Best approach and how?
	* Who is the user?
	* Quality?
	* What does Loosely Coupled mean?

###Backbone.js
####Attendees
James, Matthew, feliciA, Divya, Alex
* Everybody completed Exercise 0 from backbone class
* Started changing tests in ModelSpec.js and creating new models

##September 18, 2014
###Starting from the beginning
####Attendees
Hans, feliciA, Alex, Matt, Divya

####Summary
* reviewed the "what" and "why" of BDD and TDD
* paired up and worked until first Task on April 29, 2014

####Action Items 
* find and review TDD book Chapter 1

###Backbone.js
####Attendees
Hans, feliciA, Divya, Matthew, Kristine

####Summary
* finished the rest of Backbone.js Model slide #19 from [GDISF](http://www.teaching-materials.org/backbone/) 
* paired up to work on [exercise 0](http://www.teaching-materials.org/backbone/rolodex_exercise_steps/step0/instructions.html)
* good understanding of directory structure and process, as well as utility of Backbone.js Models

##September 11, 2014
###Attendees
James, Matthew, Felicia

###Summary
* Started learning Backbone from http://www.teaching-materials.org/backbone/ up until slide [15](http://www.teaching-materials.org/backbone/)
* Discussed the purpose and utility of Backbone Models

##September 5, 2014
###Attendees
James, Hans, Mike, Thaddeus, Matthew - maybe others but I don't remember

###Summary
* Reviewed Spies
* Matthew and James reviewed their code from spies, and completed spies exercise (see previous exercises/)

##August 22, 2014
###Attendees
Thaddeus, James, Matthew, Matt, Alex, Mike

###Summary
* Introduced Spies
* Discussed how Spies are used to examine functions with Jasmine
* Added a test using a Spy to a previous exercise
* Created a new object with complex functionality utilizing matchers we discussed before, datatype checking, and spies

###Instructions
* Start from where you left off last time, or get a starter from `previous\ exercises/072414_spies/spec`
* Scope functions to an object and update your tests
* write a spy for encodeURIComponent in the `window` scope
* make your tests pass
* practice writing other spies on methods of your object

###Instructions(2)
* Start with where we left off from the session at `previous\ exercises/082214_spies/`
* Add functionality for moving the x and y coordinates left, down, and right

##July 25, 2014 MacysUI and NavApp setup
###Attendees
Clodel, Thaddeus, feliciA, Layla, Matthew

###Summary
Set up prerequisites, configs, installations for NavApp and MacysUI

###Issues we ran into
Some documentation and links were out of date. We fixed those to the best of our ability.

We received some mvn errors from missing `settings.xml` file in `.m2` directory, which Clodel helped us debug and sent us documentation for

Latest version of Eclipse IDE Luna (4.4) does not support Java 1.6.* on Mac machines

We updated documentation wherever possible

feliciA finished number 5 of Clodel's egit instructions for eclipse setup on Kepler

Layla has MacysUI and is able to build. Next steps are getting NavApp connected

No plugins are required (except for svn) to get new Luna Eclipse IDE running

##July 17, 2014 Office Hours
###Attendees
Thaddeus, Jasmine, Hans

###Summary
* [an example of the page we looked at](http://www1.macys.com/m/campaign/sitelets/asian-pacific-heritage-month/in-store-events?cm_sp=asianpacific-_-staticpages-_-instoreeventsTopnav)
* we went over the steps together as a group and came up with the following:
	1. create a function with one argument
	2. iterate through data(JSON) and use data.categories.Event2014.entries
	3. Define variables with values not contained in the data structure
	4. Create Maps URL
	5. Call template function
* Thaddeus is going to look at rendered DOM and propose alternative templates to support semantic markup and accessibility
* At next office hours, we will write tests for 1-5
* After that, we will go over Handlebars and plug in different templates and look at them

###Instructions
* continue working with code from last week, or download a copy of [someone else's tests](https://bitbucket.org/cyberneticlove/pairedprogramming/src/4fe6a7e25abf8b41c95683276aab55450c36efe5/previous%20exercises/071014_encodeURIComponent/?at=master) 
* take a look at Matthew's code here https://gist.github.com/brendajin/fb35bbb7cab343e74c34
* with a text editor or pen and paper, work with a partner to write down the steps that the code does
* go to the URL in the ajax call and get a static copy of the JSON response. store it as a local variable in your test suite
* write a suite of tests for the steps that you wrote down. Note: you'll want to break Matthew's code up into a standalone function that takes an argument. Skip the ajax call (we'll go over that another day). 
* Refactor Matthew's code to make the tests pass!

##July 10, 2014
###Attendees
Jasmine, Layla, Matthew, Mike, Hans, Felicia, James

###Summary
* Reviewed Suite, Spec, beforeEach and afterEach
* Stepped through order of how a suite runs
* Discussed scope within tests
* Reviewed matchers `toEqual` `toBeGreaterThan` and `toBeLessThan`, as well as how to use `.not`
* Paired up and everybody did their own setup of suites and specs

###Highlights
* Practiced scoping in tests
* Practiced using anonymous functions to check for errors
* Practiced assigning local variables within a spec and verifying expectations
* Practiced mocking data

###Assignment
1. Write a function that takes a single argument that is a string
2. it should remove all spaces and replace them with '%20'
3. *Optional:* it should URI encode all other characters

* use `encodeURIComponent()`

##July 1, 2014
###Attendees 
Layla, Matthew

###Summary
Received feedback on sessions so far and how to improve.

###Going well
* rethinking how to write lean code and planning ahead

###Upcoming changes
* reschedule sessions to Thursday 11AM during Summer Hours and Friday AM during non-Summer hours
* will be taking one snippet of code and improving upon it session after session
* review Chrome dev tools – stepping in, out, and over functions with breakpoints
* review beforeEach and afterEach
* Backbone.js training – how does that fit into the larger picture
* using real-life exercises from CE code
* more hands-on macysJS
* Separate session for NavApp and macysJS environment setup

##June 24, 2014
###Attendees
Jasmine, Kristine, Thaddeus, Matthew, Hans

###Summary

* Debugged and pushed last testing suite https://bitbucket.org/cyberneticlove/myslider
* Demonstrated and practiced setting breakpoints, stepping through, into, and out of code
* Demonstrated scope inspection at breakpoints
* Demonstrated remote debugging for iOS devices
* Discussed workflow for writing tests, inspecting, stepping through, and debugging code
* Reviewed tests Brenda wrote for multi-part menu View as real-life example – emphasized starting with the most testable building blocks and moving on from there

##June 18, 2014
###Attendees
Jasmine, Kristine, Thaddeus

###Summary
It's been a while since our last meeting, because of the move. This was an informal office hours session where we reviewed the following concepts:

* `beforeEach` and `afterEach` - setting up and tearing down to prep for tests
* suite vs spec
* how to use jQuery along with tests

We wrote a great example of how to test UI using jQuery in both the test and the function. In addition, we talked about how to use testing to make sure that our updates to functions did not break existing functionality. 

Although the math and CSS on our tests is correct, the element is not animating. *Challenge* to all attendees to clone this repo: https://bitbucket.org/cyberneticlove/myslider and try to get it working! 

If anyone makes it work, they can demo at the next session.

##May 13, 2014
###Attendees
Jasmine, James, Layla

###Summary
* reviewed how Jasmine spec runner is used
* reviewed matchers
* reviewed beforeEach and afterEach
* show how jQuery can be used in tests to confirm information about the DOM

###Task
1. Start from last week's task, or start fresh by downloading the [2.0 standalone distribution](https://github.com/pivotal/jasmine/blob/master/dist/jasmine-standalone-2.0.0.zip)
2. In `lib/`, add a local version of jQuery
3. Make sure that jQuery gets loaded in `SpecRunner.html`. It should get loaded before the spec.
4. In `spec/`, Make a new `Spec.js` file -- name it whatever you want!
5. In `src/`, Make a new `.js` file -- name it whatever you want, but be consistent with the Spec name.
6. In `SpecRunner.html`, use a `<script>` tag to load your new src. Make sure it loads before the specs.
7. In `SpecRunner.html`, use a `<script>` tag to load your new spec
8. Person 1 - Write a test suite for a function called "myZoomer"
	* use jQuery in a `beforeEach` block to add an `<img>` tag to the body. Give it any `src` attribute you want.
	* use jQuery in an `afterEach` block to remove the `<img>` tag
	* write a spec for myZoomer that tests for the following:
		* it should be defined
		* it should be a function
		* it should a single string argument
		* its string argument should be either "in" or "out"
		* when you call myZoomer("in") in the spec, it should change the width of all the `<img>` elements on the page to 595px
		* when you call myZoomer("out") in the spec, it should change the width of all the `<img>` elements on the page to 1px
9. Person 2 - Write code in `src/` to pass the tests

###Action Items
* review beforeEach and afterEach again, as well as the difference between calling a function here and calling once later in a spec

##May 6, 2014
###Attendees
James, Matthew, Felicia, Kristine, Alissa

###Summary
* demo of changing data for Backbone Model
* overview of macysJS installation and setup
* identified two common components to begin testing for migration to macysJS (see below)

###Action items
* review Chrome dev tools in next session
* Matthew and Felicia will bring code examples to next office hours with the goal of testing the code, and refactoring the component to be re-usable
* Brenda and James will try to find usable version of xcode dev tools for 10.6
* Brenda will raise question of where to put re-usable CE templates in macysTemplates

###Reusable Modules Brainstorm
Matthew
* JSON parsing code - takes a JSON feed coming from a CMS in NYC, renders it into HTML, sometimes there is data parsing, sometimes the feed comes with nested objects or arrays - inconsistent data format - some randomness for data format return - sometimes there is SORT functionality on a specific field 
	* let’s use a Backbone Model to massage the data and ensure correct data format every time
	* Handlebars template for output - table or unordered list
	* Optional SORT feature
	* One time, there was a request for tool tips/layovers

Felicia
* Videos - copying and pasting a JavaScript line, and change the ID and title for the video, add class overlay “video” and add the “ID” - pulls Brand Shop API for video, and it is not customizable, limitations to visual treatment, and not HTML5 optimized - just a customized script tag
	* some parameters don’t work at all
	* create a module that accepts video and ID arguments, then returns the functionality that you actually need
	* Old code doesn’t work on certain HTML5 settings, and doesn’t work without Flash
		* No fallback code works either because of Macy’s/Brightcove integration issues
	* overall goal of re-doing the whole script to make it modern and flexible enough to change CSS, UI, on player

##April 29, 2014

###Attendees
Mike, Nick, Kristine, Felicia, Thaddeus, Matthew, James

###Summary
* tests first, code later
* Suite and spec syntax: `describe`, `it`, and `expect`
* review of matcher: `toBe`, `toEqual`, `toBeDefined`, `toThrow`
* negation: `not` for example: `expect(false).not.toBe(true);`

###Pain Points
* loading files for standalone exercises in the right order

###Task
1. Download the [2.0 standalone distribution](https://github.com/pivotal/jasmine/blob/master/dist/jasmine-standalone-2.0.0.zip)
2. In `spec/`, Make a new `Spec.js` file -- name it whatever you want!
2. In `src/`, Make a new `.js` file -- name it whatever you want, but be consistent with the Spec name.
3. In `SpecRunner.html`, add a new `<script>` tag for your new spec
3. In `SpecRunner.html`, add a new `<script>` tag for your new src. Make sure it loads before the spec.
4. Person 1 - Write a spec
	* The global variable `areDevelopersCool` should be defined
	* `areDevelopersCool` should be true
	* Global objects named `human1` and `human2` should be defined
	* `human1` and `human2` should be the same
	* A global function called `learnJasmine` should be defined
	* `learnJasmine` should not throw any errors when it is called with no arguments
	* `learnJasmine` should throw an error if any arguments are passed
5. Person 2 - Pass the tests

##April 25, 2014

###Attendees
Matthew, James, Nick

###Summary
* overview of how utilities can be deprecated, updated without breaking dependant features
* overview of how utilities are declared as dependencies with Require
* started to set up macysJS environment

###Steps
1. make sure you have the following installed:
	* git
	* [compass](http://compass-style.org/install/)
	* nodejs
	* grunt-cli
	* mvn
2. add your ssh key to [wdsgerrit](http://wdsgerrit:8080)
	* [get your ssh key](https://help.github.com/articles/generating-ssh-keys) - follow instructions until `pbcopy`, which adds the key to your clipboard. But, do not add your key to Github.
	* go to http://wdsgerrit:8080
	* on the upper-right hand side, log in
	* on the upper-right hand side, click on your name and select Settings
	* on the left-hand side, select SSH Public Keys
	* select Add Key...
	* paste your key into the text field. delete the last return carriage
	* click "add"
3. `git clone ssh://<macysId>@wdsgerrit:29418/MacysUI`
4. `cd MacysUI` (or whatever you named the directory)
5. `mvn clean install`

##April 15, 2014

###Attendees
Felicia, Jasmine, Matthew, James, Thaddeus, Layla, Nick, Mike

###Summary
1. [Survey of participants on Jasmine Unit Testing](https://docs.google.com/forms/d/1QZzR9EwlcOS3syKZqtLJsWm5BZTzcJ0Du-gAOOQUMAg/viewform?usp=send_form)
2. Skimmed introduction of [Testable JavaScript](http://shop.oreilly.com/product/0636920024699.do)
3. Verbally shared takeaways on Jasmine Testing - mainly that it does not automatically flow from BDD or TDD but is implemented for and by human beings
4. Paired up and wrote code to pass tests from a small sample math module spec that Brenda provided

###Takeaways
1. Error handling and syntax
2. Things to check for: data types, argument consistency, naming

###Questions
1. How will CE check code into macysJS if it is needed for Creative Release?
2. How does it all get put together?