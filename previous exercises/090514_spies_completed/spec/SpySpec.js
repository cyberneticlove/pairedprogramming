describe("spy007", function() {
	it("should be an object", function() {
		expect(spy007).toBeDefined();
		expect(typeof spy007).toEqual("object");
	});

	it("should have an x and y property", function() {
		expect(spy007['x']).toBeDefined();
		expect(spy007['y']).toBeDefined();
	})

	describe("move", function() {
		it("should be a method", function() {
			expect(typeof spy007.move).toEqual("function");
		})

		it("should take a string argument of 'up', 'down', 'left', or 'right'", function() {
			expect(function() {
				spy007.move('up')
			}).not.toThrow();

			expect(function() {
				spy007.move('down')
			}).not.toThrow();

			expect(function() {
				spy007.move('left')
			}).not.toThrow();

			expect(function() {
				spy007.move('right')
			}).not.toThrow();

		})

		it("should call goUp when the argument is 'up'", function() {
			spyOn(spy007, 'goUp');
			spy007.move('up');
			expect(spy007.goUp).toHaveBeenCalled();
			expect(spy007.goUp).toHaveBeenCalledWith(1);
		})

		it("should call goDown when the argument is 'down'", function() {
			spyOn(spy007, 'goDown');
			spy007.move('down');
			expect(spy007.goDown).toHaveBeenCalled();
			expect(spy007.goDown).toHaveBeenCalledWith(1);
		})
	})

	describe("goUp", function() {
		it("should be a method", function() {
			expect(typeof spy007.goUp).toEqual("function");
		});

		it("should take an argument", function() {
			expect(function() {
				spy007.goUp()
			}).toThrow();

			expect(function() {
				spy007.goUp(1)
			}).not.toThrow();

			expect(function() {
				spy007.goUp('string')
			}).toThrow();

			expect(function() {
				spy007.goUp(1, 2, 3)
			}).toThrow();
		})

		it("should increase y by 1 after it has been called", function() {
			var myY = spy007.y;
			spy007.goUp(1);
			expect(spy007.y).toEqual(myY + 1);
		})

	})

	describe("goDown", function() {
		it("should be a method", function() {
			expect(typeof spy007.goDown).toEqual("function");
		});

		it("should take an argument", function() {
			expect(function() {
				spy007.goDown()
			}).toThrow();

			expect(function() {
				spy007.goDown(1)
			}).not.toThrow();

			expect(function() {
				spy007.goDown('string')
			}).toThrow();

			expect(function() {
				spy007.goDown(1, 2, 3)
			}).toThrow();
		})

		it("should decrease y by 1 after it has been called", function() {
			var myY = spy007.y;
			spy007.goDown(1);
			expect(spy007.y).toEqual(myY - 1);
		})

	})

	describe("goRight", function() {
		it("should be a method", function() {
			expect(typeof spy007.goDown).toEqual("function");
		});

		it("should take an argument", function() {
			expect(function() {
				spy007.goDown()
			}).toThrow();

			expect(function() {
				spy007.goDown(1)
			}).not.toThrow();

			expect(function() {
				spy007.goDown('string')
			}).toThrow();

			expect(function() {
				spy007.goDown(1, 2, 3)
			}).toThrow();
		})

		it("should increase x by 1 after it has been called", function() {
			var myX = spy007.x;
			spy007.goRight(1);
			expect(spy007.x).toEqual(myX + 1);
		})

	})
})