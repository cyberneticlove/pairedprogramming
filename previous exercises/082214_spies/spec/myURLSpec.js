describe("myURL", function() {
	it("should be a function", function() {
		expect(myURL).toBeDefined();
		expect(typeof myURL).toEqual("function");
	});

	it("should require string as a parameter", function() {
		expect(function() {myURL('1') }).not.toThrow();
	});

	it("should return a string and encode uri", function() {
		var myNewString = myURL('this is gonna be fake');
		expect(myNewString).toEqual('this%20is%20gonna%20be%20fake');
	})

	it("should use encodeURIComponent", function() {
		var myString = 'some random string';
		spyOn(window, 'encodeURIComponent').and.callThrough();
		myURL(myString);
		myURL(myString);
		expect(window.encodeURIComponent).toHaveBeenCalled();
		expect(window.encodeURIComponent).toHaveBeenCalledWith(myString);
		expect(window.encodeURIComponent.calls.count()).toEqual(2);
	})
})

describe("spy007", function() {
	it("should be an object", function() {
		expect(spy007).toBeDefined();
		expect(typeof spy007).toEqual("object");
	});

	it("should have an x and y property", function() {
		expect(spy007['x']).toBeDefined();
		expect(spy007['y']).toBeDefined();
	})

	describe("move", function() {
		it("should be a method", function() {
			expect(typeof spy007.move).toEqual("function");
		})

		it("should take a string argument of 'up', 'down', 'left', or 'right'", function() {
			expect(function() {
				spy007.move('up')
			}).not.toThrow();

			expect(function() {
				spy007.move('down')
			}).not.toThrow();

			expect(function() {
				spy007.move('left')
			}).not.toThrow();

			expect(function() {
				spy007.move('right')
			}).not.toThrow();

		})

		it("should call goUp when the argument is 'up'", function() {
			spyOn(spy007, 'goUp');
			spy007.move('up');
			expect(spy007.goUp).toHaveBeenCalled();
			expect(spy007.goUp).toHaveBeenCalledWith(1);
		})
	})

	describe("goUp", function() {
		it("should be a method", function() {
			expect(typeof spy007.goUp).toEqual("function");
		});

		it("should take an argument", function() {
			expect(function() {
				spy007.goUp()
			}).toThrow();

			expect(function() {
				spy007.goUp(1)
			}).not.toThrow();

			expect(function() {
				spy007.goUp('string')
			}).toThrow();

			expect(function() {
				spy007.goUp(1, 2, 3)
			}).toThrow();
		})

		it("should increase y by 1 after it has been called", function() {
			var myY = spy007.y;
			spy007.goUp(1);
			expect(spy007.y).toEqual(myY + 1);
		})
	})
})