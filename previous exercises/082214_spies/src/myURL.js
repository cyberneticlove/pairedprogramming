var spy007 = {
	x: 0,
	y: 0,
	move: function(direction) {
		if(direction === "up") {
			this.goUp(1);
		}
	},
	goUp: function(args) {
		if(arguments.length > 1 || typeof arguments[0] !== "number") {
			throw new Error();
		}
		this.y += 1;
	}
};


var myURL = function(urlStr) {
	if ( typeof urlStr !== "string" ) {
		throw new Error("wrong datatype");
	}

	var encodedUrl = encodeURIComponent(urlStr);
	return encodedUrl;
}