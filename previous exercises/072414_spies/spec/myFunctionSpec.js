describe("myFunction", function() {
  var name;

  beforeEach(function() {
    name = "";
  });

  it("should be a function.", function() {
    expect(typeof myLibrary.myFunction).toEqual("function");
  });

  it("should be a string argument in the function.", function() {
	expect(function(){myLibrary.myFunction(name)}).not.toThrow();
  });
  
  it("should be return a encodeURI string from the function.", function() {   
  	var myNewString = myLibrary.myFunction(" jas");
  	expect(myNewString).toEqual("%20jas");
  });
});