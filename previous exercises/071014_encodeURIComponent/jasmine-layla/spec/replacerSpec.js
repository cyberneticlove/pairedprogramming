describe("urlencoder", function() {
	
	var myString = "welcome to macys";

	it("should be a function", function() {
		expect(replacer).toBeDefined();
		expect(typeof replacer).toEqual("function");
	})

	it("expects to replace spaces with %20", function() {
    var myNewString = replacer(myString);
	expect(myNewString).toEqual('welcome%20to%20macys');
	})
});
