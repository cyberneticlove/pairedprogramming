var myURL = function(urlStr) {
	if ( typeof urlStr !== "string" ) {
		throw new Error("wrong datatype");
	}

	var encodedUrl = encodeURIComponent(urlStr);
	return encodedUrl;
}