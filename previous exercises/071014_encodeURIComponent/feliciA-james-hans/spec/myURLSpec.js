describe("myURL", function() {
	it("should be a function", function() {
		expect(myURL).toBeDefined();
		expect(typeof myURL).toEqual("function");
	});

	it("should require string as a parameter", function() {
		expect(function() {myURL('1') }).not.toThrow();
	});

	it("should return a string and encode uri", function() {
		var myNewString = myURL('this is gonna be fake');
		expect(myNewString).toEqual('this%20is%20gonna%20be%20fake');
	})

})
