describe("myFunction", function() {
  var name;

  beforeEach(function() {
    name = "";
  });

  it("should be a function.", function() {
    expect(typeof myFunction).toEqual("function");
  });

  it("should be a string argument in the function.", function() {
	expect(function(){myFunction(name)}).not.toThrow();
  });
  
  it("should be return a encodeURI string from the function.", function() {   
	var myNewString = myFunction(" jas");
	expect(myNewString).toEqual("%20jas");
  });
});