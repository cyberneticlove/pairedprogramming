var Savings = Backbone.Model.extend({
	defaults: {
		originalPrice: '',
		salePrice: ''
	},
	generateSalepercent: function(){
		var salePercent = (this.get('salePrice')/this.get('originalPrice'))*100;
		return salePercent;
	},
	initialize: function(){
		this.generateSalepercent();
	}
});

