
  describe("Create a Backbone Model: ", function() {
    var salePercent;

    beforeEach(function() {
      salePercent = new Savings({originalPrice:100, salePrice: 90});
    });

    it("store it as a variable called 'Savings'", function() {
      expect(Savings).toBeDefined();
    });

    describe("give it some defaults", function() {
      var salePercent2 = new Savings();
      it("contains a default originalPrice that's empty", function() {
        expect(salePercent2.defaults.originalPrice).toBeDefined();
        expect(salePercent2.defaults.originalPrice).toEqual("");
      });
      it("contains a default salePrice that's empty", function() {
        expect(salePercent2.defaults.salePrice).toBeDefined();
        expect(salePercent2.defaults.salePrice).toEqual("");
      });
    });  
      it("contains a method called generateSalepercent", function() {
        expect(salePercent.generateSalepercent).toBeDefined();
      });
      it("generateSalepercent returns the calculated percentage", function() {
          var thePercentage;
          thePercentage = salePercent.generateSalepercent();
          expect(thePercentage).toEqual(10);
      });


  });