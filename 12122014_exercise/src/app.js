var Savings = Backbone.Model.extend({
	defaults: {
		originalPrice: '',
		salePrice: ''
	},
	generateSalepercent: function(){
		var salePercent = (this.get('salePrice')/this.get('originalPrice'))*100;
		return salePercent;
	},
	initialize: function(){
		this.generateSalepercent();
	}
});

var SavingsView = Backbone.View.extend({
	render: function() {
		this.$el.html('');
		this.$el.append('<ul><li>' + this.model.get('salePrice') + '</li><li>' + this.model.get('originalPrice') + '<li></li>' + this.model.get('salePercent') + '</li></ul>')
	}
});